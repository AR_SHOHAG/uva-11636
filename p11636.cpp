#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int x,i,j=1;
    while(scanf("%d",&x)==1 && (x>=0)){
        if(x==1)
            printf("Case %d: 0\n",j);
        for(i=0;i<14;i++)
            if(pow(2,i)<x && x<=pow(2,i+1))
                printf("Case %d: %d\n",j,i+1);
            j++;
    }
    return 0;
}

